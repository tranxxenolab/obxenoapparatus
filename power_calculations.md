Thinking through this from here: https://www.switchdoc.com/2021/08/solar-power-sizing-solar-panels-for-raspberry-pi/

Using a 6W 5V solar panel: <https://www.amazon.de/-/en/gp/product/B097CZ648C/ref=ox_sc_act_title_2?smid=A1UZ7UEONHSE8W&th=1>

6W/5V = 1200mA

Average Pi current of 0,2mA (on the conservative side, a lot more when taking photos, say 0,4mA)

Assuming 8 hours of sunlight per day, efficiency of 50% (usually we could use 70%, but this is the Netherlands......), and charging efficiency of 85%:

8 hours * 0,5 * 1200mA * 0,85 / 200mA = 20.4 hours

This is less than 24 hours, but we shut off the Pi after sunset so we should be okay. We might need a bigger solar panel at some point, but let's see how well this works.

Running the Pi for 24 hours uses 200mA / 0,85 * 24 = 5647mA. Our battery is only 5000mA, but again, we will never be running the Pi for 24 hours. This might need to be sized up int he future.


