#!/usr/bin/env python

from queue import Queue
from datetime import datetime
from random import randint

from obxeno.MQTT import ObxenoMQTT

obxenoQueue = Queue(maxsize = 400)
infoDict = {}

if __name__ == "__main__":
    obxenoMQTT = ObxenoMQTT()
    obxenoMQTT.setupMQTT()

    for i in range(0, 4):
        print(i)
        infoDict = {}
        infoDict["timestamp"] = str(datetime.now())
        infoDict["cpuTemp"] = randint(40, 80)
        infoDict["batteryLevel"] = randint(0, 100)
        infoDict["msg"] = "testing from queue: " + str(i)
        print(infoDict)
        obxenoMQTT.addToQueue(infoDict)

    obxenoMQTT.connectMQTT()
    obxenoMQTT.startMQTTLoop()

    obxenoMQTT.sendQueued()

    #obxenoMQTT.sendMessage()
    obxenoMQTT.stopMQTTLoop()
    obxenoMQTT.disconnectMQTT()
