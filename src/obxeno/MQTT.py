from configparser import ConfigParser
from queue import Queue
from collections import deque
import ssl
import json
import time
import os
import pickle
from datetime import datetime

import paho.mqtt.client as mqtt

from obxeno.ObxenoLogging import logger

topic = "obxeno/testing"
infoDict = {}

configFile = "/home/obxeno/obxeno/config.ini"
queuePickleFile = "/home/obxeno/obxeno/obxenoQueue.pkl"
dequePickleFile = "/home/obxeno/obxeno/obxenoDeque.pkl"

config = ConfigParser()
config.read(configFile)

if "MQTT" in config.sections():
    if "username" in config["MQTT"].keys():
        username = config["MQTT"]["username"]
    else:
        logger.error("Unable to find username for MQTT server, quitting...")
        sys.exit(1)

    if "password" in config["MQTT"].keys():
        password = config["MQTT"]["password"]
    else:
        logger.error("Unable to find password for MQTT server, quitting...")
        sys.exit(1)



broker = "mqtt.eclipseprojects.io"
#port = 8883
port = 1883

def on_publish(client, userdata, mid):
    print("sent a message")

"""
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, "obxeno")

#client.username_pw_set(username, password)
#client.tls_set(certfile=None,
#    keyfile=None,
#    cert_reqs=ssl.CERT_REQUIRED)


client.on_publish = on_publish
client.connect(broker, port)
# start a new thread
client.loop_start()

msg = "hello from python"
infoDict["timestamp"] = str(datetime.now())
infoDict["cpuTemp"] = 40.0
infoDict["batteryLevel"] = 82
infoDict["msg"] = "testing from class"

info = client.publish(
    topic='obxeno/data',
    #payload=msg.encode('utf-8'),
    payload=json.dumps(infoDict).encode("utf-8"),
    qos=1,
)
# Because published() is not synchronous,
# it returns false while he is not aware of delivery that's why calling wait_for_publish() is mandatory.
info.wait_for_publish()
print(info.is_published())
client.loop_stop()
client.disconnect()
"""

class ObxenoMQTT(object):

    def __init__(self, configFile = "/home/obxeno/obxeno/config.ini", queueLength = 4000, queuePickleFile = queuePickleFile, dequeLength = 4000, dequePickleFile = dequePickleFile):
        self.logger = logger
        self.queuePickleFile = queuePickleFile
        self.queueLength = queueLength
        self.dequePickleFile = dequePickleFile
        self.dequeLength = dequeLength


        # Check if we have a pickle file
        if os.path.exists(dequePickleFile):
            with open(self.dequePickleFile, 'rb') as f:
                self.obxenoDeque = pickle.load(f)
        else:
            self.obxenoQueue = Queue(maxsize = queueLength)
            self.obxenoDeque = deque(maxlen = queueLength)

        self.configFile = configFile
        self.config = ConfigParser()
        self.config.read(self.configFile)
        
        if "MQTT" in self.config.sections():
            if "username" in self.config["MQTT"].keys():
                self.username = self.config["MQTT"]["username"]
            else:
                self.logger.error("Unable to find username for MQTT server, quitting...")
                sys.exit(1)
        
            if "password" in self.config["MQTT"].keys():
                self.password = self.config["MQTT"]["password"]
            else:
                self.logger.error("Unable to find password for MQTT server, quitting...")
                sys.exit(1)

            if "broker" in self.config["MQTT"].keys():
                self.broker = self.config["MQTT"]["broker"]
            else:
                self.logger.error("Unable to find broker for MQTT server, quitting...")
                sys.exit(1)

            if "port" in self.config["MQTT"].keys():
                self.port = int(self.config["MQTT"]["port"])
            else:
                self.logger.error("Unable to find port for MQTT server, quitting...")
                sys.exit(1)

            if "ssl" in self.config["MQTT"].keys():
                self.ssl = int(self.config["MQTT"]["ssl"])
            else:
                self.logger.error("Unable to find ssl setting for MQTT server, quitting...")
                sys.exit(1)


            if "location" in self.config["MQTT"].keys():
                self.location = str(self.config["MQTT"]["location"])
            else:
                self.logger.error("Unable to find location setting for MQTT server, quitting...")
                sys.exit(1)



    def addToQueue(self, item):
        if (self.obxenoQueue.full()):
            # Throw away the first item if we're full
            self.obxenoQueue.get()
            self.obxenoQueue.put(item)
        else:
            self.obxenoQueue.put(item)

    def addToDeque(self, item):
        if (len(self.obxenoDeque) == self.dequeLength):
            self.obxenoDeque.pop()
            self.obxenoDeque.appendleft(item)
        else:
            self.obxenoDeque.appendleft(item)

    def removeQueuePickleFile(self):
        os.remove(self.queuePickleFile)

    def removeDequePickleFile(self):
        os.remove(self.dequePickleFile)

    def getLocation(self):
        return self.location

    def saveQueueToPickleFile(self):
        with open(self.queuePickleFile, 'wb') as f:
            pickle.dump(self.obxenoQueue, f)

    def saveDequeToPickleFile(self):
        with open(self.dequePickleFile, 'wb') as f:
            pickle.dump(self.obxenoDeque, f)


    def setupMQTT(self):
        self.client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, "obxeno")
        if (self.ssl):
            self.client.username_pw_set(username, password)
            self.client.tls_set(certfile=None,
                keyfile=None,
                cert_reqs=ssl.CERT_REQUIRED)
            #self.client.tls_insecure_set(True)

    def connectMQTT(self):
        self.client.on_publish = self.on_publish
        self.client.connect(self.broker, self.port)

    def startMQTTLoop(self):
        self.client.loop_start()

    def stopMQTTLoop(self):
        self.client.loop_stop()

    def disconnectMQTT(self):
        self.client.disconnect()

    def on_publish(self, client, userdata, mid):
        print("sent a message")

    def sendQueued(self):
        while True:
            item = self.obxenoQueue.get()

            self.sendMessage(message = item)
            self.saveQueueToPickleFile()

            if (self.obxenoQueue.empty()):
                self.removeQueuePickleFile()
                break

    def sendDequed(self):
        while True:
            item = self.obxenoDeque.pop()

            self.sendMessage(message = item)
            self.saveDequeToPickleFile()

            if (len(self.obxenoDeque) == 0):
                self.removeDequePickleFile()
                break


    def sendMessage(self, message = "testing"):
        info = self.client.publish(
            topic='obxeno/data',
            #payload=msg.encode('utf-8'),
            payload=json.dumps(message).encode("utf-8"),
            qos=1,
        )
        # Because published() is not synchronous,
        # it returns false while he is not aware of delivery that's why calling wait_for_publish() is mandatory.
        info.wait_for_publish()
        #print(info.is_published())


if __name__ == "__main__":
    obxenoMQTT = ObxenoMQTT()
    print(obxenoMQTT.broker)
    obxenoMQTT.setupMQTT()
    obxenoMQTT.connectMQTT()
    obxenoMQTT.startMQTTLoop()
    obxenoMQTT.sendMessage()
    obxenoMQTT.stopMQTTLoop()
    obxenoMQTT.disconnectMQTT()
