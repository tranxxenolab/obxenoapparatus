import time
import sys
from datetime import datetime, timedelta
import asyncio
import subprocess
import json

import libcamera
from picamera2 import Picamera2, Preview
from gpiozero import CPUTemperature
from pijuice import PiJuice
import ephem

from obxeno.ObxenoLogging import logger
from obxeno.MQTT import ObxenoMQTT
from obxeno.Wifi import Wifi
from obxeno.Victron import ObxenoVictronScanner

#picam2.set_controls({"AnalogueGain": 8.0, "ExposureTime": 200, "AeEnable": False, "AwbEnable": True, "AwbMode": libcamera.controls.AwbModeEnum.Daylight, "FrameRate": 1.0/frameWait})
#picam2.set_controls({"AnalogueGain": 6.0, "ExposureTime": 600, "AeEnable": False, "AwbEnable": True, "AwbMode": libcamera.controls.AwbModeEnum.Daylight})
#picam2.set_controls({"AeEnable": True, "AeConstraintMode": libcamera.controls.AeConstraintModeEnum.Normal, "AeExposureMode": libcamera.controls.AeExposureModeEnum.Normal, "AeMeteringMode": libcamera.controls.AeMeteringModeEnum.CentreWeighted, "ExposureValue": 1.00, "AwbEnable": True, "AwbMode": libcamera.controls.AwbModeEnum.Daylight})
# This is for color temp of 6500K
#picam2.set_controls({"AeEnable": True, "AeConstraintMode": libcamera.controls.AeConstraintModeEnum.Normal, "AeExposureMode": libcamera.controls.AeExposureModeEnum.Normal, "AeMeteringMode": libcamera.controls.AeMeteringModeEnum.CentreWeighted, "ExposureValue": 1.00, "ColourGains": (3.740720510482788, 1.5019958019256592)})


PICAM_CONTROLS_DICT = {
    "AeEnable": True, 
    "AeConstraintMode": libcamera.controls.AeConstraintModeEnum.Normal, 
    "AeExposureMode": libcamera.controls.AeExposureModeEnum.Normal, 
    "AeMeteringMode": libcamera.controls.AeMeteringModeEnum.CentreWeighted, 
    "ExposureValue": 1.00, 
    #"ExposureTime": 10000000,
    # This is for color temp of 6500K
    "ColourGains": (3.740720510482788, 1.5019958019256592)
}

PICAM_NIGHTTIME_CONTROLS_DICT = {
    #"AeEnable": True, 
    #"AeConstraintMode": libcamera.controls.AeConstraintModeEnum.Normal, 
    #"AeExposureMode": libcamera.controls.AeExposureModeEnum.Normal, 
    #"AeMeteringMode": libcamera.controls.AeMeteringModeEnum.CentreWeighted, 
    "AnalogueGain": 8,
    "ExposureValue": 1.00, 
    "ExposureTime": 10000000,
    # This is for color temp of 6500K
    "ColourGains": (3.740720510482788, 1.5019958019256592)
}


#DEFAULT_FRAMEWAIT = 30.0
#DEFAULT_FRAMEWAIT = 300.0
DEFAULT_FRAMEWAIT = 900.0
#DEFAULT_FRAMEWAIT = 180.0

DEFAULT_BLE_WAIT = 10.0

DEFAULT_LOW_BATTERY = 12.05
DEFAULT_LOW_BATTERY_COUNT = 3

PIJUICE_BATTERY_HIGH = 64
PIJUICE_BATTERY_LOW = 40

FILE_STEM = "/home/obxeno/obxeno_data/obxeno_"

class Camera(object):
    def __init__(self, frameWait = DEFAULT_FRAMEWAIT):
        self.logger = logger
        self.logger.info("Starting camera module")

        self.obxenoMQTT = ObxenoMQTT()
        self.obxenoMQTT.setupMQTT()

        self.scanner = ObxenoVictronScanner()
        self.loop = asyncio.get_event_loop()
        #self.loop.run_until_complete(self.scanStartStop())
        #self.logger.debug(self.scanner.getReceivedData())


        self.infoDict = {}
        self.infoDict['timestamp'] = str(datetime.now())
        self.infoDict['message'] = "Starting camera module"
        #self.infoDict['solar'] = self.scanner.getReceivedData()
        self.obxenoMQTT.addToDeque(self.infoDict)
        self.infoDict = {}

        self.wifi = Wifi()

        self.lowBatteryCount = 0


        self.sun = ephem.Sun()
        self.amsterdam = ephem.Observer()
        self.amsterdam.lat = "52.318906"
        self.amsterdam.lon = "4.977936"
        self.amsterdam.elevation = 0

        self.pj = PiJuice(1, 0x14)
        self.picam2 = Picamera2()
        #self.capture_config = self.picam2.create_still_configuration(raw={})
        self.capture_config = self.picam2.create_still_configuration(main={"size": (4056, 3040), "format": "RGB888"})

        self.picam2.configure(self.capture_config)

        self.frameWait = frameWait
        
        self.setPicamControls()

    async def scanStartStop(self):
        await self.scanner.start()
        await asyncio.sleep(DEFAULT_BLE_WAIT)
        await self.scanner.stop()


    async def scanStartStopCollectData(self):
        await self.scanner.start()
        await asyncio.sleep(DEFAULT_BLE_WAIT)
        await self.scanner.stop()
        
        self.infoDict = {}
        self.infoDict["timestamp"] = str(datetime.now())
        self.infoDict["cpuTemp"] = CPUTemperature().temperature
        self.infoDict["batteryLevel"] = self.pj.status.GetChargeLevel()['data']
        self.infoDict["frame"] = self.frame
        self.infoDict["location"] = self.obxenoMQTT.getLocation()
        self.logger.debug(self.scanner.getReceivedData())
        self.infoDict['solar'] = self.scanner.getReceivedData()


    def setPicamControls(self, controlsDict = PICAM_CONTROLS_DICT):
        self.logger.info("Setting controls using the following dict: {}".format(controlsDict))
        self.picam2.set_controls(controlsDict)

    def setPicamOptions(self):
        self.logger.info("Setting picam options, like compression level for pngs...")
        self.picam2.options["compress_level"] = 6

    def startCamera(self):
        self.logger.debug("Starting picam2...")
        self.picam2.start()
        time.sleep(2)

    def takeFrame(self, stem):
        self.logger.debug("Taking frame.")
        try:
            self.logger.debug("Before capture_request")
            r = self.picam2.capture_request()
            r.save("main", FILE_STEM + stem + ".png")
            r.release()
            self.logger.debug("After capture_request")
        except Exception as e:
            self.logger.error(e)

    def cameraLoop(self):
        self.frame = 0
        # Every interval frames, check some things
        interval = 1
        firstRun = True

        while True:
            # TODO
            # Ensure that we start at the top of the minute
            # first run, wait until we've reaching the beginning of the next minute, then start
            # disable this wait for future loops
            now = datetime.now()
            self.amsterdam.date = now
            self.sun.compute(self.amsterdam)
            if (self.sun.alt > 0):
                self.setPicamControls(controlsDict = PICAM_CONTROLS_DICT)
            else:
                self.setPicamControls(controlsDict = PICAM_NIGHTTIME_CONTROLS_DICT)

            if firstRun:
                if ((now.minute + 1) > 59):
                    then = datetime(now.year, now.month, now.day, now.hour + 1, 0, 0, 0)
                else:
                    then = datetime(now.year, now.month, now.day, now.hour, now.minute + 1, 0, 0)
                sleepTime = (then - now).total_seconds()
                self.logger.debug("Sleeping for {} seconds to wait until the start of the next minute....".format(sleepTime))
                if (sleepTime > 0):
                    time.sleep(sleepTime)
                now = datetime.now()
                firstRun = False
            timestamp = "{:%Y%m%d%H%M%S}".format(now)
            self.takeFrame(timestamp)
            #r = self.picam2.capture_request()
            #r.save("main", FILE_STEM + timestamp + ".png")
            #r.release()
            after = datetime.now()
            exposureTime = after - now
            if ((self.frame % interval) == 0):
                cpuTemp = CPUTemperature().temperature
                self.logger.debug("Current CPU temp: {}".format(cpuTemp))
                pijuiceBatteryLevel = self.pj.status.GetChargeLevel()['data']
                self.logger.debug("Current pijuice battery charge level: {}".format(pijuiceBatteryLevel))

                if (pijuiceBatteryLevel > PIJUICE_BATTERY_HIGH):
                    self.pj.config.SetChargingConfig({'charging_enabled': False}, False)
                    self.logger.debug("Disabling Pijuice battery charging")
                elif (pijuiceBatteryLevel < PIJUICE_BATTERY_LOW):
                    self.pj.config.SetChargingConfig({'charging_enabled': True}, True)
                    self.logger.debug("Enabling Pijuice battery charging")


                pijuiceCharging = self.pj.config.GetChargingConfig()['data']['charging_enabled']
                self.infoDict = {}
                self.infoDict["timestamp"] = str(datetime.now())
                self.infoDict["cpuTemp"] = CPUTemperature().temperature
                self.infoDict["batteryLevel"] = self.pj.status.GetChargeLevel()['data']
                self.infoDict["frame"] = self.frame
                self.infoDict["location"] = self.obxenoMQTT.getLocation()
                self.infoDict["charging"] = pijuiceCharging

                self.loop.run_until_complete(self.scanStartStopCollectData())

                #self.obxenoMQTT.addToQueue(self.infoDict)
                self.obxenoMQTT.addToDeque(self.infoDict)

                # Check on battery level
                #j = json.loads(self.infoDict["solar"])
                batteryLevel = self.infoDict["solar"]["payload"]["battery_voltage"]

                # Increment the low battery count if we're below the threshold
                # But if we go above the threshold, reset it
                if (batteryLevel < DEFAULT_LOW_BATTERY):
                    self.lowBatteryCount += 1
                else:
                    self.lowBatteryCount = 0

                # If the number of cycles in a row that we're low on battery
                # is above the threshold, turn the device off until tomorrow
                if (self.lowBatteryCount >= DEFAULT_LOW_BATTERY_COUNT):
                    self.logger.error("SHUTTING DOWN DUE TO LOW BATTERY")
                    subprocess.run(['/home/obxeno/bin/do_shutdown_tasks.sh 1'])

                if (self.wifi.checkWifiNetworks()):
                    try:
                        self.logger.info("Trying to send data to MQTT server")
                        self.obxenoMQTT.connectMQTT()
                        self.obxenoMQTT.startMQTTLoop()

                        #self.obxenoMQTT.sendQueued()
                        self.obxenoMQTT.sendDequed()

                        self.obxenoMQTT.stopMQTTLoop()
                        self.obxenoMQTT.disconnectMQTT()
                    except Exception as e:
                        self.logger.warn("Unable to send data to MQTT server: " + str(e))
                        self.logger.warn("Saving data to pickle file instead")
                        self.obxenoMQTT.saveDequeToPickleFile()
                else:
                    self.obxenoMQTT.saveDequeToPickleFile()



            self.frame += 1
            self.logger.debug("Sleeping for " + str(self.frameWait - DEFAULT_BLE_WAIT - exposureTime.total_seconds()))
            sleepTime = self.frameWait - DEFAULT_BLE_WAIT - exposureTime.total_seconds()
            if (sleepTime > 0):
                time.sleep(sleepTime)
