import subprocess

from obxeno.ObxenoLogging import logger

ALLOWED_NETWORKS=["ASK hotspot"]

class Wifi(object):
    def __init__(self):
        self.logger = logger
        self.logger.info("Starting wifi check routines...")

        self.allowed = False

    def checkWifiNetworks(self):
        foundCount = 0
        foundNetwork = ""
        # TODO this might be a bit brittle :(
        networkName = subprocess.check_output(['/usr/sbin/iwgetid', 'wlan0']).split(b':')[1].decode("utf-8").split("\"")[1] 
        for name in ALLOWED_NETWORKS:
            if name == networkName:
                foundCount += 1
                foundNetwork = name

        if (foundCount > 0):
            self.logger.debug("Associated with allowed network: " + foundNetwork)
            self.allowed = True
        else:
            self.allowed = False

        return self.allowed

if __name__=="__main__":
    wifi = Wifi()
    wifi.checkWifiNetworks()
