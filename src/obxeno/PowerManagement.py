import os
import time
from datetime import datetime, timezone, timedelta
import subprocess
import sys

import dateutil.parser
from dateutil import tz
from pijuice import PiJuice
import ephem
from crontab import CronTab

from obxeno.ObxenoLogging import logger

USER = 'obxeno'
COMMAND_PATH = '/home/obxeno/bin/do_shutdown_tasks.sh 3'

class PowerManagement(object):
    def __init__(self, lat = '42.615606', lon = '-94.140400'):
        self.logger = logger
        self.logger.info("Starting power management routines...")
        self.lat = lat
        self.lon = lon
        self.pj = PiJuice(1, 0x14)
        self.logger.info("Initializing with lat, lon: {}, {}".format(self.lat, self.lon))

    def setup(self):
        subprocess.call(["sudo", "hwclock", "--hctosys"])

        self.calcEphem()

    def calcEphem(self):
        self.sun = ephem.Sun()
        self.obs = ephem.Observer()
        self.obs.lat = self.lat
        self.obs.lon = self.lon
        # This gives us a time in UTC
        #self.next_rising = self.obs.next_rising(self.sun).datetime()
        self.next_rising = self.obs.next_rising(self.sun).datetime() + timedelta(hours=0)
        #self.next_setting = self.obs.next_setting(self.sun).datetime()
        self.next_setting = self.obs.next_setting(self.sun).datetime() - timedelta(hours=0)
        self.next_rising_dict = {
            'second': self.next_rising.second,
            'minute': self.next_rising.minute,
            #'minute': 15,
            'hour': self.next_rising.hour,
            #'hour': 13,
            'day': self.next_rising.day,
            #'day': 11,
            'month': self.next_rising.month,
            'year': self.next_rising.year
        }
        self.logger.info("Next sunrise for location: {}".format(self.next_rising))
        self.logger.info("Next sunset for location: {}".format(self.next_setting))

    def checkPijuiceStatus(self, pijuiceResponse):
        if pijuiceResponse['error'] != 'NO_ERROR':
            self.logger.error("pijuice failed with code: {}".format(pijuiceResponse['error']))
            raise RuntimeError("pijuice failed with code: {}".format(pijuiceResponse['error']))

    def setWakeup(self):
        wakeupTime = datetime.now() + timedelta(minutes=2)
        wakeupTimeDict = {
            'second': wakeupTime.second,
            'minute': wakeupTime.minute,
            #'minute': 15,
            'hour': wakeupTime.hour,
            #'hour': 13,
            'day': wakeupTime.day,
            #'day': 11,
            'month': wakeupTime.month,
            'year': wakeupTime.year
        }

        #self.logger.debug(wakeupTimeDict)
        #status = self.pj.rtcAlarm.SetAlarm(wakeupTimeDict)
        status = self.pj.rtcAlarm.SetAlarm(self.next_rising_dict)
        self.checkPijuiceStatus(status)
        self.logger.debug("Setting RTC alarm")

    def setCrontab(self):
        self.logger.info("Setting up crontab for shutdown tasks...")
        self.cron = CronTab(user=USER)
        # Set shutdown with a cron job
        results = self.cron.find_command(COMMAND_PATH)
        
        for item in results:
            self.logger.debug("Removing old shutdown items")
            self.cron.remove(item)
        
        self.logger.debug("Setting and writing cron entry")
        """
        job = self.cron.new(command=COMMAND_PATH)
        job.minute.on(self.next_setting.minute)
        job.hour.on(self.next_setting.hour)
        self.cron.write()
        """
        job = self.cron.new(command=COMMAND_PATH)
        now = datetime.now()

        if ((ephem.next_full_moon(now).datetime() - now).days == 0):
            self.logger.info("Full moon tonight...delaying shutdown to the following day")
            self.logger.info("May Obxeno be illuminated by these nocturnal rays.")
            # NOTE: this is not technically correct...we should create an observer
            # for the next day and calcuate the setting time for that day
            # We cheat a little bit by assuming it's not that different from
            # today's setting time.
            self.next_setting = self.obs.next_setting(self.sun).datetime() + timedelta(days=1)
            job.minute.on(self.next_setting.minute)
            job.hour.on(self.next_setting.hour)
            job.day.on(self.next_setting.day)
            job.month.on(self.next_setting.month)
            self.logger.debug(self.next_setting)
        else:
            job.minute.on(self.next_setting.minute)
            job.hour.on(self.next_setting.hour)
	
        self.cron.write()

    def turnOff(self):
        self.pj.config.SetChargingConfig({'charging_enabled': False}, False)
        self.pj.rtcAlarm.ClearAlarmFlag()
        self.pj.rtcAlarm.SetWakeupEnabled(True)
        self.logger.info("Turning off, after setting wakeup enabled")
        time.sleep(0.4)
        self.pj.power.SetPowerOff(60)
        subprocess.call(["sudo", "shutdown", "-h", "now"])
