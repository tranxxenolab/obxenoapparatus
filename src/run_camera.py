#!/usr/bin/env python

from obxeno.Camera import Camera 


if __name__ == "__main__":
    c = Camera()
    c.setPicamOptions()
    c.startCamera()
    try:
        c.cameraLoop()
    except Exception as e:
        c.logger.error("Some kind of error: {}".format(e))
