#!/usr/bin/env python

import datetime
import os
import time
from datetime import datetime, timezone, timedelta
import dateutil.parser
from dateutil import tz
from pijuice import PiJuice
import argparse

import ephem

from crontab import CronTab

def pijuice_check_error(pyjuice_response):
    if pyjuice_response['error'] != 'NO_ERROR':
        raise RuntimeError("pijuice failed with code: {}".format(pyjuice_response['error']))

# offset is in seconds
utc_offset = lambda offset: timezone(timedelta(seconds=offset))

sun = ephem.Sun()
amsterdam = ephem.Observer()
amsterdam.lat = '52.318906'
amsterdam.lon = '4.977936'
# This gives us a time in UTC
next_rising = amsterdam.next_rising(sun).datetime()
next_setting = amsterdam.next_setting(sun).datetime()

# Set wakeup

#wake_dt = dateutil.parser.parse(timestr=next_rising).replace(tzinfo=dateutil.tz.tzlocal())

print("Setting alarm to UTC: {}".format(next_rising))
next_rising_dict = {'second': next_rising.second,
    'minute': next_rising.minute,
    'hour': next_rising.hour,
    'day': next_rising.day,
    'month': next_rising.month,
    'year': next_rising.year}

pj = PiJuice(1, 0x14)
pj.rtcAlarm.SetWakeupEnabled(True)
pijuice_check_error(pj.rtcAlarm.SetAlarm(next_rising_dict))

cron = CronTab(user='obxeno')
# Set shutdown with a cron job
results = cron.find_command('sudo /usr/sbin/shutdown -h now')

for item in results:
    print("Removing old shutdown items")
    cron.remove(item)

print("Setting and writing cron entry")
print("Shutting down now.")
job = cron.new(command='sudo /usr/sbin/shutdown -h now')
job.minute.on(next_setting.minute)
job.hour.on(next_setting.hour)
cron.write()
