#!/usr/bin/python3

import time
from datetime import datetime, timedelta

import libcamera
from picamera2 import Picamera2, Preview

import ephem

sun = ephem.Sun()
amsterdam = ephem.Observer()
amsterdam.lat = '52.318906'
amsterdam.lon = '4.977936'
amsterdam.next_setting(sun).datetime()

# How many seconds to wait between frames
frameWait = 30.0

picam2 = Picamera2()
#picam2.start_preview(Preview.QTGL)

#preview_config = picam2.create_preview_configuration()
capture_config = picam2.create_still_configuration(raw={})
picam2.configure(capture_config)
#picam2.set_controls({"AnalogueGain": 8.0, "ExposureTime": 200, "AeEnable": False, "AwbEnable": True, "AwbMode": libcamera.controls.AwbModeEnum.Daylight, "FrameRate": 1.0/frameWait})
#picam2.set_controls({"AnalogueGain": 6.0, "ExposureTime": 600, "AeEnable": False, "AwbEnable": True, "AwbMode": libcamera.controls.AwbModeEnum.Daylight})
#picam2.set_controls({"AeEnable": True, "AeConstraintMode": libcamera.controls.AeConstraintModeEnum.Normal, "AeExposureMode": libcamera.controls.AeExposureModeEnum.Normal, "AeMeteringMode": libcamera.controls.AeMeteringModeEnum.CentreWeighted, "ExposureValue": 1.00, "AwbEnable": True, "AwbMode": libcamera.controls.AwbModeEnum.Daylight})
# This is for color temp of 6500K
picam2.set_controls({"AeEnable": True, "AeConstraintMode": libcamera.controls.AeConstraintModeEnum.Normal, "AeExposureMode": libcamera.controls.AeExposureModeEnum.Normal, "AeMeteringMode": libcamera.controls.AeMeteringModeEnum.CentreWeighted, "ExposureValue": 1.00, "AnalogueGain": 8, "ColourGains": (3.740720510482788, 1.5019958019256592)})

picam2.start()
time.sleep(2)

frame = 0
while True:
    # TODO
    # Ensure that we start at the top of the minute
    # first run, wait until we've reaching the beginning of the next minute, then start
    # disable this wait for future loops
    now = datetime.now()
    timestamp = "{:%Y%m%d%H%M%S}".format(now)
    print("Taking frame " + str(frame))
    r = picam2.capture_request()
    #print(dir(r))
    #print(r.get_metadata())
    r.save("main", "/home/obxeno/obxeno_data/obxeno_" + timestamp + ".png")
    r.release()
    after = datetime.now()
    exposureTime = after - now
    frame += 1
    print("Sleeping for " + str(frameWait - exposureTime.total_seconds()))
    time.sleep(frameWait - exposureTime.total_seconds())
