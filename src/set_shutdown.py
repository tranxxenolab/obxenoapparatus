#!/usr/bin/env python

from obxeno.PowerManagement import PowerManagement

if __name__ == "__main__":
    #pm = PowerManagement()
    pm = PowerManagement(lat = '52.318906', lon = '4.977936')
    pm.setup()
    pm.setCrontab()
