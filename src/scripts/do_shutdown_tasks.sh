#!/bin/bash


source /home/obxeno/obxeno/bin/activate
PARENT_COMMAND=$(ps -o args= $PPID)
echo "---" >>  /home/obxeno/do_wakeup_shutdown.log
echo $PARENT_COMMAND >> /home/obxeno/do_wakeup_shutdown.log

if [[ $1 == 0 ]]; then
    echo "Called from the command-line" >> /home/obxeno/do_wakeup_shutdown.log

elif [[ $1 == 1 ]]; then
    echo "Called from Camera.py code" >> /home/obxeno/do_wakeup_shutdown.log

elif [[ $1 == 2 ]]; then
    echo "Called from pijuice configuration program" >> /home/obxeno/do_wakeup_shutdown.log

elif [[ $1 == 3 ]]; then
    echo "Called from cron" >> /home/obxeno/do_wakeup_shutdown.log

else
    echo "Unknown calling location" >> /home/obxeno/do_wakeup_shutdown.log

fi
echo "shutting down" >> /home/obxeno/do_wakeup_shutdown.log
date >> /home/obxeno/do_wakeup_shutdown.log
python3 /home/obxeno/obxeno/src/set_wakeup_and_shutdown.py
#sudo shutdown -h now
