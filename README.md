# The Obxeno Apparatus

Source code for this project.

# PiJuice Zero

Need to pay attention to the info in this bug report: <https://github.com/PiSupply/PiJuice/issues/1063>.

Remember to enable I2C using raspi-config.

Install i2c-tools.

Use i2cdetect -y 1 to ensure that address 0x68 reads UU.

Be sure to add this to `/boot/config.txt`: `dtoverlay=i2c-rtc,ds1307`.

# WIFI

From: <https://raspberrypi.stackexchange.com/questions/11631/how-to-setup-multiple-wifi-networks>

You can also run wpa_passphrase my_SSID my_password and it will return the config needed to put into /etc/wpa_supplicant/wpa_supplicant.conf – 
Madacol
May 21, 2022 at 18:36
2
One-liner: sudo sh -c 'wpa_passphrase my_SSID my_password >> /etc/wpa_supplicant/wpa_supplicant.conf' – 
Madacol
May 21, 2022 at 18:36   
